FROM python:3.7.12-alpine3.15

COPY . /webapps/ipsum
WORKDIR /webapps/ipsum

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "/webapps/ipsum/manage.py", "runserver", "0.0.0.0:8000"]
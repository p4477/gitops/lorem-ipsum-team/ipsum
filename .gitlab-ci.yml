stages:
  - "Setup"
  - "Build"
  - "Deploy"
  - "Cleanup"

# ==================================================================================================================== #
#                                                 Feature Branch
# ==================================================================================================================== #

deploy-fb-helm-charts:
  stage: "Setup"
  tags:
    - k8s
    - helm
  only:
    # Regex matching branches create via Gitlab Issues
    - /^[0-9]+-([0-9a-zA-Z-]+)+$/
  script:
    # Checks if there is already a Helm installation of Lorem Ipsum
    - export EXISTING_RELEASE=$(helm ls -n $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME | grep $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME | wc -l)
    - helm repo update
    # Install Lorem Ipsum helm charts if not already installed
    - > 
      if [ ${EXISTING_RELEASE} -eq 0 ]; then
        helm install $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME lorem-ipsum/lorem-ipsum --namespace=$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME --create-namespace --set-string subdomain=$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME;
      else 
        helm status -n $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME;
      fi

build-fb-image:
  stage: "Build"
  tags:
    - docker
  only:
    - /^[0-9]+-([0-9a-zA-Z-]+)+$/
  script:
    - docker build -t registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHA --file=Dockerfile .
    - docker push registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHA

deploy-fb:
  stage: "Deploy"
  tags:
    - k8s
  only:
    - /^[0-9]+-([0-9a-zA-Z-]+)+$/
  script:
    - kubectl -n $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME set image deployment ipsum ipsum=registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHA
    - kubectl -n $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME rollout status deploy/ipsum

# ==================================================================================================================== #
#                                                 Development branch
# ==================================================================================================================== #
build-dev-image:
  stage: "Build"
  tags:
    - docker
  only:
    - development
  script:
    # Build new deployment branch image
    - docker build -t registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:development-$CI_COMMIT_SHA --file=Dockerfile .
    - docker push registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHA
    # Tag new build as development-latest
    - docker tag registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHA registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:development-latest
    - docker push registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:development-latest

deploy-development:
  stage: "Deploy"
  tags:
    - k8s
  only:
    - development
  script:
    - kubectl -n development set image deployment ipsum ipsum=registry.gitlab.com/p4477/gitops/lorem-ipsum-team/ipsum:development-$CI_COMMIT_SHA
    - kubectl -n development rollout status deploy/ipsum

# Deletes the helm deployment of the feature branch that is merged into development branch
cleanup-helm:
  stage: "Cleanup"
  tags:
    - k8s
    - helm
  only:
    - development
  # This step is not that critical, it is allowed to fail
  # Failure can occur when some cowboy commits directly into development branch with using gitlab issues and feature branch
  allow_failure: true
  script:
    # We are unable to retrieve the $CI_COMMIT_REF_NAME from the pipeline.
    # However, $CI_COMMIT_TITLE for Merge Requests is always in the format of `Merge branch '$CI_COMMIT_REF_NAME' into 'development'`
    # We can deduce the $CI_COMMIT_REF_NAME from the $CI_COMMIT_TITLE
    - export SOURCE_BRANCH_NAME=$(echo $CI_COMMIT_TITLE | cut -d ' ' -f 3 | sed s/\'//g)
    - helm uninstall $CI_PROJECT_NAME-$SOURCE_BRANCH_NAME -n $CI_PROJECT_NAME-$SOURCE_BRANCH_NAME
    - kubectl delete ns $CI_PROJECT_NAME-$SOURCE_BRANCH_NAME
